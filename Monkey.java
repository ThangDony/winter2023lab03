public class Monkey {

	public String breed;
	public String size;
	public int longevity;

	public void climb() {
		System.out.println("OOOOH OOOOH AAAAH AAAAH, I CLIMB TREES");
	}
	public void swim() {
		if (size.toLowerCase() == "large") {
			System.out.println("I can probably not swim :(");
		} else {
			System.out.println("I can most likely swim :)");
		}
	}

}