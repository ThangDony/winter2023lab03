public class Application {

	public static void main(String[] args) {
		
		Monkey monkey1 = new Monkey();
		monkey1.longevity = 32;
		// System.out.println("Lifespan: " + monkey1.longevity);
		monkey1.breed = "Chimpanzee";
		// System.out.println(monkey1.breed);
		monkey1.size = "large";
		// System.out.println(monkey1.size);
		monkey1.climb();
		monkey1.swim();

		System.out.println("------------------------------------------");

		Monkey monkey2 = new Monkey();
		monkey2.longevity = 20;
		// System.out.println("Lifespan: " + monkey2.longevity);
		monkey2.breed = "proboscis";
		// System.out.println("Breed: " + monkey2.breed);
		monkey2.size = "small";
		// System.out.println("Size: " + monkey2.size);
		monkey2.climb();
		monkey2.swim();

		System.out.println("------------------------------------------");

		Monkey[] troop = new Monkey[3];
		
		troop[0] = monkey1;
		troop[1] = monkey2;

		troop[2] = new Monkey();
		troop[2].longevity = 40;
		troop[2].breed = "Gorilla";
		troop[2].size = "large";

		// Use to print one of the fields that we want from whichever troop of monkey
		System.out.println(troop[0].breed);
 
		
		
	}
}